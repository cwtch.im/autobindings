module git.openprivacy.ca/cwtch.im/autobindings

go 1.23.1

toolchain go1.23.2

require (
	cwtch.im/cwtch v0.29.4
	git.openprivacy.ca/cwtch.im/server v1.6.1
	git.openprivacy.ca/openprivacy/connectivity v1.11.1
	git.openprivacy.ca/openprivacy/log v1.0.3
	github.com/mutecomm/go-sqlcipher/v4 v4.4.2
)

require (
	filippo.io/edwards25519 v1.0.0 // indirect
	git.openprivacy.ca/cwtch.im/tapir v0.6.0 // indirect
	git.openprivacy.ca/openprivacy/bine v0.0.5 // indirect
	github.com/gtank/merlin v0.1.1 // indirect
	github.com/gtank/ristretto255 v0.1.3-0.20210930101514-6bb39798585c // indirect
	github.com/mimoo/StrobeGo v0.0.0-20220103164710-9a04d6ca976b // indirect
	go.etcd.io/bbolt v1.3.6 // indirect
	golang.org/x/crypto v0.28.0 // indirect
	golang.org/x/mobile v0.0.0-20230531173138-3c911d8e3eda // indirect
	golang.org/x/mod v0.21.0 // indirect
	golang.org/x/net v0.30.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
	golang.org/x/tools v0.26.0 // indirect
)
