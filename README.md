# Cwtch Autobindings

Automatically generate [Cwtch](https://git.openprivacy.ca/cwtch.im/cwtch) C-Bindings from a high-level specification.

Note for the Flutter-based Cwtch UI Application see: https://git.openprivacy.ca/cwtch.im/cwtch-ui

## Building

	go mod download
	go run generate/generate_bindings.go
	make linux  // alternatively windows, android, macos

### Experiments

Autobindings allow for comile time selection of experiments. Default selections for each target platform are supplied in Makefile but can be overridden with:

	env EXPERIMENTS="serverExperiment otherExperiment" make android

## Spec File Format

The current Cwtch Bindings Specification is defined in [spec](./spec)

Supported function prototypes:

* `app <Function Name> <Args>` - an application-level function, called on global `Application`
* `profile <Function Name> <Args>` - a profile-level function, takes `profile` as an implicit first argument
* `json(profile) <Function Name> <Args>` - a profile-level function, takes `profile` as an implicit first argument and returns a json blob that must be freed by the calling library (see [MEMORY](./MEMORY.md))
* `@profile-experiment <Function Name> <Experiment> <Args>`- an experimental profile-level function, takes `profile` as an implicit first argument, experiment must implement cwtch Functionality interface
* `@(json)profile-experiment <Function Name> <Experiment> <Args>` - am experimental profile-level function, takes `profile` as an implicit first argument and returns a json blob that must be freed by the calling  (see [MEMORY](./MEMORY.md)) library, , experiment must implement cwtch Functionality interface

Supported argument prototypes:

* `profile` - a cwtch profile identifier
* `conversation` - a cwtch conversation identifier
* `message` - a cwtch message identifier
* `int:<name>` - a generic integer argument (with optional name)
* `bool:<name>` - a generic boolean argument (with optional name)
* `string:<name>` - a generic string argument (with optional name)

All arguments must be defined in the order they are specified by the underlying Cwtch library function.

Other directives:

* `import <go pkg>` - includes an additional go importin the compiled lib.go (needed for experiments)
* Functions that start with `Enhanced` are automatically stripped of that prefix for their binding names e.g. `EnhancedGetMessage` generated a binding `c_GetMessage` - for certain functions Cwtch has two potential calling options `<Function>` and `Enhanced<Function>`. "Enhanced" in this case means that the response is optimised for calling by a user-facing application by e.g. directly making a number of calls under the hood and returning a json blob of the results.

# Using

## General Environment Variables

- `LOG_FILE` if defined will mean all go logging will go to a file instead of stdout
- `LOG_LEVEL` if set to `debug` will cause debug logging to be included in log output
- `CWTCH_PROFILE` if set to `1` will cause a memory profile to be written to `mem.prof` and all active goroutines
  written to `stdout` when `DebugInfo()` is called.
- `CWTCH_TAILS` if set to `true` will override the Tor configuration to allow Cwtch to work in a Tail-like environment.

## Linux Desktop:

- `LD_LIBRARY_PATH` set to point to `libCwtch.so`
- or drop a symlink into `/usr/lib`

### Cross compile arm 64

Needs `sudo apt install gcc-aarch64-linux-gnu`

## Android

- copy `cwtch.aar` into `flutter_app/android/cwtch`

## Windows

- copy libCwtch.dll into the directory of the `.exe` using it

## MacOS

- copy libCwtch.x64.dylib and libCwtch.arm.dylib into the directory you are executing from

## Support Cwtch Development

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

