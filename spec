# ( app | profile) FunctionName	(profile)

# Peer Engine
app ActivatePeerEngine profile
app DeactivatePeerEngine profile
app ConfigureConnections profile bool:listen bool:peers bool:servers

# Profile Management
(string)app CreateProfile name password bool:autostart
app LoadProfiles password
app DeleteProfile profile password
(json)app EnhancedImportProfile string:file password
profile ChangePassword string:current string:newPassword string:newPasswordAgain
profile ExportProfile string:file

# Decorator Interfaces Across experiments
import "cwtch.im/cwtch/functionality/inter"
@(json)profile-experiment EnhancedImportBundle inter string:bundle
@(json)profile-experiment EnhancedSendMessage inter conversation string:msg

# Conversation Management
profile ArchiveConversation conversation
profile AcceptConversation conversation
profile BlockConversation conversation
profile UnblockConversation conversation
profile DeleteConversation conversation
profile PeerWithOnion string:handle
profile DisconnectFromPeer string:handle
(json-err)profile EnhancedGetConversationAccessControlList conversation
profile EnhancedUpdateConversationAccessControlList conversation string:json

# Search
(json)profile SearchConversations string:pattern

# Message Management
(json)profile EnhancedGetMessageById conversation message
(json)profile EnhancedGetMessageByContentHash conversation string:contentHash
(json)profile EnhancedGetMessages conversation int:index uint:count
(json)profile EnhancedSendInviteMessage conversation conversation:target
profile UpdateMessageAttribute conversation channel message string:attributeKey string:attributeValue

# Group Management
profile StartGroup string:name string:server
profile QueueJoinServer string:handle
profile DisconnectFromServer string:handle

## Server List Management...
import "cwtch.im/cwtch/functionality/servers"
@profile-experiment PublishServerUpdate servers 
@profile-experiment GetServerInfoList servers
@profile-experiment DeleteServerInfo servers string:serverOnion

# Filesharing Management
import "cwtch.im/cwtch/functionality/filesharing"
@profile-experiment DownloadFileDefaultLimit filesharing conversation string:filepath string:manifest string:filekey
@profile-experiment RestartFileShare filesharing string:filekey
@profile-experiment StopFileShare filesharing string:filekey
@profile-experiment CheckDownloadStatus filesharing string:filekey
@profile-experiment VerifyOrResumeDownloadDefaultLimit filesharing conversation string:filekey
@(json)profile-experiment EnhancedShareFile filesharing conversation string:filepath
@(json)profile-experiment EnhancedGetSharedFiles filesharing conversation


# Server Hosting Experiment
!serverExperiment import "git.openprivacy.ca/cwtch.im/autobindings/experiments/server_hosting"
!serverExperiment global serverExperiment *server_hosting.ServersFunctionality server_hosting
!serverExperiment exp CreateServer application password string:description bool:autostart
!serverExperiment exp SetServerAttribute application string:handle string:key string:val
!serverExperiment exp LoadServers application acn password
!serverExperiment exp LaunchServers application acn
!serverExperiment exp LaunchServer application string:handle
!serverExperiment exp StopServer application string:handle
!serverExperiment exp StopServers application
!serverExperiment exp DestroyServers
!serverExperiment exp DeleteServer application string:handle password
