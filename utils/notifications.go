package utils

import (
	"cwtch.im/cwtch/model"
	"cwtch.im/cwtch/model/attr"
	"cwtch.im/cwtch/settings"
	"git.openprivacy.ca/cwtch.im/autobindings/constants"
)

func determineNotification(ci *model.Conversation, gsettings settings.GlobalSettings) constants.NotificationType {
	switch gsettings.NotificationPolicy {
	case settings.NotificationPolicyMute:
		return constants.NotificationNone
	case settings.NotificationPolicyOptIn:
		if ci != nil {
			if policy, exists := ci.GetAttribute(attr.LocalScope, attr.ProfileZone, constants.ConversationNotificationPolicy); exists {
				switch policy {
				case constants.ConversationNotificationPolicyDefault:
					return constants.NotificationNone
				case constants.ConversationNotificationPolicyNever:
					return constants.NotificationNone
				case constants.ConversationNotificationPolicyOptIn:
					return notificationContentToNotificationType(gsettings.NotificationContent)
				}
			}
		}
		return constants.NotificationNone
	case settings.NotificationPolicyDefaultAll:
		if ci != nil {
			if policy, exists := ci.GetAttribute(attr.LocalScope, attr.ProfileZone, constants.ConversationNotificationPolicy); exists {
				switch policy {
				case constants.ConversationNotificationPolicyNever:
					return constants.NotificationNone
				case constants.ConversationNotificationPolicyDefault:
					fallthrough
				case constants.ConversationNotificationPolicyOptIn:
					return notificationContentToNotificationType(gsettings.NotificationContent)
				}
			}
		}
		return notificationContentToNotificationType(gsettings.NotificationContent)
	}
	return constants.NotificationNone
}

func notificationContentToNotificationType(notificationContent string) constants.NotificationType {
	if notificationContent == "NotificationContent.ContactInfo" {
		return constants.NotificationConversation
	}
	return constants.NotificationEvent
}
