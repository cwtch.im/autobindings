package constants

const Name = "name"

const Picture = "picture"
const DefaultProfilePicture = "defaultPicture"

const Archived = "archived"
const LastSeenTime = "lastMessageSeenTime"

// PeerOnline stores state on if the peer believes it is online
const PeerOnline = "peer-online"

const PeerAutostart = "autostart"

// ConversationNotificationPolicy is the attribute label for conversations. When App NotificationPolicy is OptIn a true value here opts in
const ConversationNotificationPolicy = "notification-policy"
